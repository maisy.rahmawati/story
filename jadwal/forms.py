from django import forms
from .models import Jadwal
from django.forms import ModelForm

class JadwalForm(ModelForm):
    class Meta:
        model = Jadwal
        fields = ['title','day','date','time','location','category','description']
        labels = {'title':'Title','day':'Day','date':'Date','time':'Time','location':'Location','category':'Category','description':'Description'}
        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'day':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'date':forms.DateInput(attrs={'class':'form-control','type':'date'}),
            'time':forms.TimeInput(attrs={'class':'form-control','type':'time'}),
            'location':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'category':forms.Select(attrs={'class':'form-control'}),
            'description':forms.Textarea(attrs={'class':'form-control','type':'textarea','row':'10'}),
            }
