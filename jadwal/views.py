from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from .forms import JadwalForm
from .models import Jadwal
from django.shortcuts import redirect
import datetime


# Create your views here.
def add_jadwal(request):
    if request.method == "POST":
        form = JadwalForm(request.POST)
        print(form.is_valid())
        print(form.errors)
        if form.is_valid():
            response = {}
            response['title'] = request.POST['title']
            response['day'] = request.POST['day']
            response['date'] = request.POST['date']
            response['time'] = request.POST['time']
            response['location'] = request.POST['location']
            response['category'] = request.POST['category']
            response['description'] = request.POST['description']
            jadwal_item = Jadwal(title=response['title'], day=response['day'],date=response['date'],time=response['time'],location=response['location'],category=response['category'],description=response['description'])
            jadwal_item.save()
            return redirect(reverse('jadwal:add_jadwal'))
    else:
        form = JadwalForm()
    return render(request, 'jadwal_form.html', {'title':'Form Jadwal','form': form})


def edit_jadwal_save(request, id=None):
    item = Jadwal.objects.get(id=id)
    if request.method == "POST":
        form = JadwalForm(request.POST, instance=item) 
        if form.is_valid():
            form.save()
            return redirect(reverse('jadwal:edit_jadwal_save'))
    else:
        form = JadwalForm(instance=post)
        return redirect(reverse('jadwal:view_jadwal'))
    #return render(request, 'jadwal/jadwal_form.hrml', {'form': form})


def edit_jadwal(request, id=id):
    item = Jadwal.objects.get(id=id)
    form = JadwalForm(instance=item)
    return render(request, 'jadwal_edit.html', {'title':'Form Jadwal','form':form,'item':item})


def view_jadwal(request):
    data = Jadwal.objects.all()
    return render(request, 'jadwal_view.html', {'data': data})


def display_jadwal(request, id=id):
    item = Jadwal.objects.get(id=id)
    return render(request, 'jadwal_display.html', {'item':item})


def delete_jadwal(request, pk):
    Jadwal.objects.filter(pk = pk).delete()
    db_item = Jadwal.objects.all()
    content = {'title':'Jadwal', 'db_item':db_item}
    return redirect('/jadwal/view_jadwal/')
    
    
   
