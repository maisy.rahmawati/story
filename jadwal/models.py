from django.db import models
import datetime

# Create your models here.
CATEGORY=[('College', 'College'),
          ('Organization', 'Organization'),
          ('Comitee', 'Comitee'),
          ('Family', 'Family'),
          ('Refreshing', 'Refreshing'),
          ('Me Time', 'Me Time'),
          ('Others', 'Others')]

class Jadwal(models.Model):
    title = models.CharField(max_length=100)
    day = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=100)
    category = models.CharField(max_length=100, choices=CATEGORY)
    description = models.CharField(null=True, max_length=100)
