from django.conf.urls import url
from . import views
from django.shortcuts import render
from django.urls import path

app_name = 'jadwal'
urlpatterns = [
    path('add_jadwal/', views.add_jadwal, name='add_jadwal'),
    path('edit_jadwal_save/<int:id>', views.edit_jadwal_save, name='edit_jadwal_save'),
    path('edit_jadwal/<int:id>', views.edit_jadwal, name='edit_jadwal'),
    path('view_jadwal/', views.view_jadwal, name='view_jadwal_semua'),
    path('view_jadwal/<int:id>', views.display_jadwal, name='view_jadwal'),
    path('delete_jadwal/<int:pk>', views.delete_jadwal, name='delete_jadwal'),
]
